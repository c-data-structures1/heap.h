/**
 * Copyright (C) 2020 Fabio Sussarellu
 *
 * This file is part of CDataStructures.
 *
 * CDataStructures is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CDataStructures is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CDataStructures.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Heap.h"
#include "Array.h/Array.c"

struct Heap {
    Array A;
    uint Items;
    int (*compare)(Item a, Item b);
};

// * STATIC
static uint leftItem(uint i) { return i * 2 + 1; }

static uint rightItem(uint i) { return i * 2 + 2; }

static uint parentItem(uint i) { return (i - 1) / 2; }
// * END OF STATIC

Heap Heap__new(uint max, int (*compare)(Item a, Item b), void (*free)(Item i),
               void (*print)(Item i)) {
    Heap h = malloc(sizeof(*h));
    h->A   = Array__new(free, print);
    Array__allocate(h->A, max);
    h->compare = compare;
    h->Items   = 0;
    return h;
}

void Heap__free(Heap h) {
    Array__free(h->A, true);
    free(h);
}

void Heap__add(Heap h, Item i) {
    Array__add(h->A, i);
    h->Items++;
}

void Heap__swap(Heap h, uint index1, uint index2) {
    Array__swap(h->A, index1, index2);
}

void Heap__heapify(Heap h, uint index) {
    uint left, right, largest;
    left  = leftItem(index);
    right = rightItem(index);
    if (left < h->Items &&
        h->compare(h->A->Items[left], h->A->Items[index]) == 1)
        largest = left;
    else
        largest = index;
    if (right < h->Items &&
        h->compare(h->A->Items[right], h->A->Items[largest]) == 1)
        largest = right;
    if (largest != index) {
        Heap__swap(h, index, largest);
        Heap__heapify(h, largest);
    }
}

void Heap__build(Heap h) {
    for (int i = (h->Items) / 2 - 1; i >= 0; i--)
        Heap__heapify(h, i);
}

void Heap__sort(Heap h) {
    int i, j;
    Heap__build(h);
    j = h->Items;
    for (i = h->Items - 1; i > 0; i--) {
        Heap__swap(h, 0, i);
        h->Items--;
        Heap__heapify(h, 0);
    }
    h->Items = j;
}

void Heap__delete(Heap h, uint index) {
    Array__delete(h->A, index);
    h->Items--;
}

void Heap__print(Heap h) { Array__print(h->A); }

uint Heap__size(Heap h) { return h->Items; }

uint Heap__maxSize(Heap h) { return h->A->ItemsNumber; };
