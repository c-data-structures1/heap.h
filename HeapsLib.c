/**
 * Copyright (C) 2020 Fabio Sussarellu
 *
 * This file is part of CDataStructures.
 *
 * CDataStructures is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CDataStructures is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CDataStructures.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "HeapsLib.h"
#include "Heap.c"

HeapsLib newHeapsLib() {
    HeapsLib l;

    l.add     = Heap__add;
    l.build   = Heap__build;
    l.delete  = Heap__delete;
    l.free    = Heap__free;
    l.heapify = Heap__heapify;
    l.new     = Heap__new;
    l.sort    = Heap__sort;
    l.swap    = Heap__swap;
    l.print   = Heap__print;
    l.size    = Heap__size;
    l.maxSize = Heap__maxSize;

    return l;
}