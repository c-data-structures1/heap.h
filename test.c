/**
 * Copyright (C) 2020 Fabio Sussarellu
 *
 * This file is part of CDataStructures.
 *
 * CDataStructures is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CDataStructures is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CDataStructures.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "HeapsLib.h"

HeapsLib Heaps;
uint MAX_ITEMS = 15;

typedef int *Int;
Int newInt(int value) {
    Int i = malloc(sizeof(*i));
    *i    = value;
    return i;
}

int compare(Int a, Int b) {
    if (*a == *b) { return 0; }
    if (*a < *b) { return -1; }
    if (*a > *b) { return 1; }
}

void printInt(Int i) { printf("%d ", *i); }

int main(int argc, char const *argv[]) {
    Heaps = newHeapsLib();

    Heap h = Heaps.new(MAX_ITEMS, (void *)&compare, (void *)&free,
                       (void *)&printInt);

    for (size_t i = 0; i < MAX_ITEMS - 1; i++) {
        Heaps.add(h, newInt(i));
    }

    Heaps.build(h);
    Heaps.print(h);
    Heaps.sort(h);
    Heaps.print(h);
    Heaps.free(h);
    return 0;
}
