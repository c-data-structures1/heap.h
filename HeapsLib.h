/**
 * Copyright (C) 2020 Fabio Sussarellu
 *
 * This file is part of CDataStructures.
 *
 * CDataStructures is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CDataStructures is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CDataStructures.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HEAPSLIB_H
#define HEAPSLIB_H

// * DNT
#include "Heap.h"

typedef struct HeapsLib {
    void (*delete)(Heap h, uint index);
    void (*sort)(Heap h);
    void (*build)(Heap h);
    void (*heapify)(Heap h, uint index);
    void (*swap)(Heap h, uint index1, uint index2);
    void (*add)(Heap h, Item i);
    void (*free)(Heap h);
    Heap (*new)(uint max, int (*compare)(Item a, Item b), void (*free)(Item i),
                void (*print)(Item i));
    void (*print)(Heap h);
    uint (*size)(Heap h);
    uint (*maxSize)(Heap h);
} HeapsLib;

// * EDNT

HeapsLib newHeapsLib();

#endif // ! HEAPSLIB_H
